console.log("JS OOP Capstone Activity");

class Customer {
  constructor(email) {
    this.email = email;
    this.cart = new Cart();
    this.orders = [];
  }

  checkout() {
    let cartItem = {
      products: this.cart.contents,
      totalAmount: this.cart.totalAmount,
    };

    if (this.cart.contents.length != 0) {
      this.orders.push(cartItem);
      console.log("Successful checkout!");
    } else {
      console.log("No items in cart!");
    }

    return this;
  }
}

class Cart {
  constructor() {
    this.contents = [];
    this.totalAmount = 0;
  }

  addToCart(productName, quantity) {
    let product = {
      product: productName,
      quantity: quantity,
    };

    this.contents.push(product);
    console.log("Product is successfully added to cart.");

    return this;
  }

  showCartContents() {
    return this.contents;
  }

  updateProductQuantity(name, quantity) {
    const index = this.contents.findIndex(
      (content) => content.product.name === name
    );

    if (index === -1) {
      console.log(`${name} does not exist in cart.`);
    } else {
      this.contents[index].quantity = quantity;
      console.log("Quantity is successfully updated.");
    }

    return this;
  }

  clearCartContents() {
    this.contents = [];
    console.log("Cart contents now empty.");
    return this;
  }

  computeTotal() {
    this.totalAmount = 0;

    this.contents.forEach((item) => {
      this.totalAmount = this.totalAmount + item.quantity * item.product.price;
    });

    return this;
  }
}

class Product {
  constructor(name, price) {
    this.name = name;
    this.price = price;
    this.isActive = true;
  }

  archive() {
    if (this.isActive) {
      this.isActive = false;
      console.log("Product is successfully archived.");
    } else {
      console.log("Product is already archived.");
    }

    return this;
  }

  updatePrice(price) {
    this.price = price;
    console.log("Product price is successfully updated.");

    return this;
  }

  activate() {
    if (!this.isActive) {
      this.isActive = true;
      console.log("Product is successfully activated.");
    } else {
      console.log("Product is already activated.");
    }

    return this;
  }
}

const weng = new Customer("weng@gmail.com");

const productA = new Product("Pizza", 240);
const productB = new Product("Fries", 55);
